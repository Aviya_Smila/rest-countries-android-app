package com.example.restcountries.utils.network;

import com.example.restcountries.model.Country;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface RestCountriesService {
    @GET(NetworkingConstants.GET_ALL_COUNTRIES_ENDPOINT)
    Observable<List<Country>> listCountries();
}
