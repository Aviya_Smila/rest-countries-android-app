package com.example.restcountries.utils.network;

public class NetworkingConstants {
    public static final String BASE_URL = "https://restcountries.eu";
    public static final String GET_ALL_COUNTRIES_ENDPOINT = "/rest/v2/all";
}
