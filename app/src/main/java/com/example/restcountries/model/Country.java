package com.example.restcountries.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Country implements Serializable {
    private String name;
    ArrayList<String> topLevelDomain = new ArrayList<String>();
    private String alpha2Code;
    private String alpha3Code;
    ArrayList<String> callingCodes = new ArrayList<String>();
    private String capital;
    ArrayList<String> altSpellings = new ArrayList<String>();
    private String region;
    private String subregion;
    private float population;
    ArrayList<Double> latlng = new ArrayList<Double>();
    private String demonym;
    private float area;
    private float gini;
    ArrayList<String> timezones = new ArrayList<String>();
    ArrayList<String> borders = new ArrayList<String>();
    private String nativeName;
    private String numericCode;
    ArrayList<Currency> currencies = new ArrayList<Currency>();
    ArrayList<Language> languages = new ArrayList<Language>();
    Translations TranslationsObject;
    private String flag;
    ArrayList<RegionalBloc> regionalBlocs = new ArrayList<RegionalBloc>();
    private String cioc;
    ArrayList<Country> neighbourCountries = new ArrayList<Country>();


    // Getter Methods
    public String getName() {
        return name;
    }

    public String getAlpha2Code() {
        return alpha2Code;
    }

    public String getAlpha3Code() {
        return alpha3Code;
    }

    public String getCapital() {
        return capital;
    }

    public String getRegion() {
        return region;
    }

    public String getSubregion() {
        return subregion;
    }

    public float getPopulation() {
        return population;
    }

    public String getDemonym() {
        return demonym;
    }

    public float getArea() {
        return area;
    }

    public float getGini() {
        return gini;
    }

    public String getNativeName() {
        return nativeName;
    }

    public String getNumericCode() {
        return numericCode;
    }

    public Translations getTranslations() {
        return TranslationsObject;
    }

    public String getFlag() {
        return flag;
    }

    public ArrayList<String> getBorders() {
        return borders;
    }

    public String getCioc() {
        return cioc;
    }

    public ArrayList<Country> getNeighbourCountries() {
        return neighbourCountries;
    }

    // Setter Methods

    public void setName(String name) {
        this.name = name;
    }

    public void setAlpha2Code(String alpha2Code) {
        this.alpha2Code = alpha2Code;
    }

    public void setAlpha3Code(String alpha3Code) {
        this.alpha3Code = alpha3Code;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setSubregion(String subregion) {
        this.subregion = subregion;
    }

    public void setPopulation(float population) {
        this.population = population;
    }

    public void setDemonym(String demonym) {
        this.demonym = demonym;
    }

    public void setArea(float area) {
        this.area = area;
    }

    public void setGini(float gini) {
        this.gini = gini;
    }

    public void setNativeName(String nativeName) {
        this.nativeName = nativeName;
    }

    public void setNumericCode(String numericCode) {
        this.numericCode = numericCode;
    }

    public void setTranslations(Translations translationsObject) {
        this.TranslationsObject = translationsObject;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public void setCioc(String cioc) {
        this.cioc = cioc;
    }

    public void setNeighbourCountries(ArrayList<Country> neighbourCountries) {
        this.neighbourCountries = neighbourCountries;
    }
}
class Translations implements Serializable {
    private String de;
    private String es;
    private String fr;
    private String ja;
    private String it;
    private String br;
    private String pt;


    // Getter Methods

    public String getDe() {
        return de;
    }

    public String getEs() {
        return es;
    }

    public String getFr() {
        return fr;
    }

    public String getJa() {
        return ja;
    }

    public String getIt() {
        return it;
    }

    public String getBr() {
        return br;
    }

    public String getPt() {
        return pt;
    }

    // Setter Methods

    public void setDe(String de) {
        this.de = de;
    }

    public void setEs(String es) {
        this.es = es;
    }

    public void setFr(String fr) {
        this.fr = fr;
    }

    public void setJa(String ja) {
        this.ja = ja;
    }

    public void setIt(String it) {
        this.it = it;
    }

    public void setBr(String br) {
        this.br = br;
    }

    public void setPt(String pt) {
        this.pt = pt;
    }
}

class Currency implements Serializable {
    private String code;
    private String name;
    private String symbol;


    // Getter Methods

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getSymbol() {
        return symbol;
    }

    // Setter Methods

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }
}

class Language implements Serializable {
    private String iso639_1;
    private String iso639_2;
    private String name;
    private String nativeName;


    // Getter Methods

    public String getIso639_1() {
        return iso639_1;
    }

    public String getIso639_2() {
        return iso639_2;
    }

    public String getName() {
        return name;
    }

    public String getNativeName() {
        return nativeName;
    }

    // Setter Methods

    public void setIso639_1(String iso639_1) {
        this.iso639_1 = iso639_1;
    }

    public void setIso639_2(String iso639_2) {
        this.iso639_2 = iso639_2;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNativeName(String nativeName) {
        this.nativeName = nativeName;
    }
}
class RegionalBloc implements Serializable {
    private String acronym;
    private String name;
    ArrayList < String > otherAcronyms = new ArrayList < String > ();
    ArrayList < String > otherNames = new ArrayList < String > ();


    // Getter Methods

    public String getAcronym() {
        return acronym;
    }

    public String getName() {
        return name;
    }

    // Setter Methods

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public void setName(String name) {
        this.name = name;
    }
}