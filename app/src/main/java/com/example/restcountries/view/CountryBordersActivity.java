package com.example.restcountries.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.example.restcountries.R;
import com.example.restcountries.databinding.ActivityCountryBordersBinding;
import com.example.restcountries.model.Country;
import com.example.restcountries.view.adapter.CountryAdapter;
import com.example.restcountries.viewmodel.CountriesDisplayViewModel;
import com.example.restcountries.viewmodel.CountryBorderViewModel;

import java.util.Observable;
import java.util.Observer;

public class CountryBordersActivity extends AppCompatActivity implements Observer {

    private static final String EXTRA_COUNTRY = "EXTRA_COUNTRY" ;

    private ActivityCountryBordersBinding activityCountryBordersBinding;
    private CountryBorderViewModel countryBorderViewModel;

    public static Intent getIntent(Context context, Country country) {
        Intent intent = new Intent(context, CountryBordersActivity.class);
        intent.putExtra(EXTRA_COUNTRY, country);
        return intent;
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_borders);
        initDataBinding();
        setUpListOfCountriesView(activityCountryBordersBinding.countriesRecyclerView);
        setUpObserver(countryBorderViewModel);
    }

    private void initDataBinding() {
        Country country = (Country) getIntent().getSerializableExtra(EXTRA_COUNTRY);
        activityCountryBordersBinding = DataBindingUtil.setContentView(this, R.layout.activity_country_borders);
        countryBorderViewModel = new CountryBorderViewModel(country);
        activityCountryBordersBinding.setCountryBorderViewModel(countryBorderViewModel);
    }

    // set up the list of country with recycler view
    private void setUpListOfCountriesView(RecyclerView countryRecycler) {
        CountryAdapter countryAdapter = new CountryAdapter();
        countryRecycler.setAdapter(countryAdapter);
        countryRecycler.setLayoutManager(new LinearLayoutManager(this));
    }

    public void setUpObserver(Observable observable) {
        observable.addObserver(this);
    }

    @Override
    public void update(Observable o, Object arg) {
        if(o instanceof CountryBorderViewModel) {
            CountryAdapter countryAdapter = (CountryAdapter) activityCountryBordersBinding.countriesRecyclerView.getAdapter();
            CountryBorderViewModel countryBorderViewModel = (CountryBorderViewModel) o;
            countryAdapter.setCountryList(countryBorderViewModel.getCountriesList());
        }
    }
}
