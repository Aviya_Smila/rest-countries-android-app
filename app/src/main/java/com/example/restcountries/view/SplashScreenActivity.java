package com.example.restcountries.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.restcountries.R;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
    }

    public void onClick(View v){
        Intent MainActivity = new Intent(getApplicationContext(),CountriesDisplayActivity.class);
        startActivity(MainActivity);
        finish();
    }

}
