package com.example.restcountries.view.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;


import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import com.example.restcountries.R;
import com.example.restcountries.databinding.CountryRowTableBinding;
import com.example.restcountries.model.Country;
import com.example.restcountries.viewmodel.CountryItemViewModel;

import java.util.Collections;
import java.util.List;

public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.CountryAdapterViewHolder> {

    private List<Country> countryList;

    public CountryAdapter() {this.countryList = Collections.emptyList();}

    @Override
    public CountryAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        CountryRowTableBinding itemUserBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.country_row_table ,parent, false);
        return new CountryAdapterViewHolder(itemUserBinding);
    }

    @Override
    public void onBindViewHolder(CountryAdapterViewHolder holder, int position) {
        holder.bindCountry(countryList.get(position));

    }

    @Override
    public int getItemCount() {
        return  countryList.size();
    }

    public void setCountryList(List<Country> countryList) {
        this.countryList = countryList;
        notifyDataSetChanged();
    }

    public static class CountryAdapterViewHolder extends RecyclerView.ViewHolder {

        CountryRowTableBinding mItemCountryBinding;

        public CountryAdapterViewHolder(CountryRowTableBinding itemUserBinding) {
            super(itemUserBinding.itemPeople);
            this.mItemCountryBinding = itemUserBinding;
        }

        void bindCountry(Country country){
            if(mItemCountryBinding.getCountryViewModel() == null){
                mItemCountryBinding.setCountryViewModel(new CountryItemViewModel(country, itemView.getContext()));
            }
            else {
                mItemCountryBinding.getCountryViewModel().setCountry(country);
            }
        }
    }
}