package com.example.restcountries.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import com.example.restcountries.R;
import com.example.restcountries.databinding.ActivityMainBinding;
import com.example.restcountries.view.adapter.CountryAdapter;
import com.example.restcountries.viewmodel.CountriesDisplayViewModel;

import java.util.Observable;
import java.util.Observer;


public class CountriesDisplayActivity extends AppCompatActivity implements Observer {

    private ActivityMainBinding countryActivityBinding;
    private CountriesDisplayViewModel countryViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDataBinding();
        setUpListOfCountriesView(countryActivityBinding.countriesRecyclerView);
        setUpObserver(countryViewModel);
    }

    private void initDataBinding() {
        countryActivityBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        countryViewModel = new CountriesDisplayViewModel(this);
        countryActivityBinding.setCountriesDisplayViewModel(countryViewModel);
    }

    // set up the list of country with recycler view
    private void setUpListOfCountriesView(RecyclerView countryRecycler) {
        CountryAdapter countryAdapter = new CountryAdapter();
        countryRecycler.setAdapter(countryAdapter);
        countryRecycler.setLayoutManager(new LinearLayoutManager(this));
    }

    public void setUpObserver(Observable observable) {
        observable.addObserver(this);
    }

    @Override
    public void update(Observable o, Object arg) {
        if(o instanceof CountriesDisplayViewModel) {
            CountryAdapter countryAdapter = (CountryAdapter) countryActivityBinding.countriesRecyclerView.getAdapter();
            CountriesDisplayViewModel countryViewModel = (CountriesDisplayViewModel) o;
            countryAdapter.setCountryList(countryViewModel.getCountriesList());
        }
    }

    @Override protected void onDestroy() {
        super.onDestroy();
        countryViewModel.reset();
    }

    public void openSortingOptions(View v){
        findViewById(R.id.sortingoptions).setVisibility(View.VISIBLE);
    }
    public void closeSortingOptions(View v){
        findViewById(R.id.sortingoptions).setVisibility(View.GONE);
    }

}
