package com.example.restcountries.viewmodel;

import android.app.Activity;
import android.content.Context;
import androidx.databinding.BaseObservable;
import androidx.databinding.BindingAdapter;

import android.view.View;
import android.widget.ImageView;

import com.ahmadrosid.svgloader.SvgLoader;
import com.example.restcountries.R;
import com.example.restcountries.model.Country;
import com.example.restcountries.view.CountryBordersActivity;


public class CountryItemViewModel extends BaseObservable {

    private Country country;
    private Context context;

    public CountryItemViewModel(Country country, Context context){
        this.country = country;
        this.context = context;
    }

    public String getImageURL() {
        return country.getFlag();
    }

    public String getName(){
        return country.getName();
    }

    public String getNativeName(){
        return country.getNativeName();
    }

    public String getAreaSize(){
        return "Area size: " + (int)country.getArea();
    }
    // Loading Image using Glide Library.
    @BindingAdapter("imageUrl") public static void setImageUrl(ImageView imageView, String url){
        SvgLoader.pluck()
                .with((Activity) imageView.getContext())
                .setPlaceHolder(R.mipmap.ic_launcher, R.mipmap.ic_launcher)
                .load(url, imageView);
    }


    public void onItemClick(View v){
        context.startActivity(CountryBordersActivity.getIntent(v.getContext(), country));
    }

    public void setCountry(Country country) {
        this.country = country;
        notifyChange();
    }
}