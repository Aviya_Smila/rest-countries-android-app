package com.example.restcountries.viewmodel;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableInt;

import com.example.restcountries.R;
import com.example.restcountries.model.Country;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class CountryBorderViewModel extends Observable {
    public ObservableInt progressBar;
    public ObservableInt countryRecycler;
    private List<Country> countryList;
    private Country country;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public CountryBorderViewModel(@NonNull Country country) {
        this.country = country;
        this.countryList = new ArrayList<>();
        this.progressBar = new ObservableInt(View.GONE);
        this.countryRecycler = new ObservableInt(View.VISIBLE);
        updateCountriesDataList();
    }

    private void updateCountriesDataList() {
        Disposable disposable =
                io.reactivex.Observable.just(country.getNeighbourCountries()).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Consumer<List<Country>>() {
                            @Override public void accept(List<Country> Response) throws Exception {
                                countryList.addAll(Response);
                                progressBar.set(View.GONE);
                                setChanged();
                                notifyObservers();
                            }
                        }, new Consumer<Throwable>() {
                            @Override public void accept(Throwable throwable) throws Exception {
                                progressBar.set(View.GONE);
                                countryRecycler.set(View.GONE);
                            }
                        });
        compositeDisposable.add(disposable);
    }

    public String getName(){
        return country.getName();
    }

    public String getImageURL() {
        return country.getFlag();
    }

    public List<Country> getCountriesList() {
        return countryList;
    }
}
