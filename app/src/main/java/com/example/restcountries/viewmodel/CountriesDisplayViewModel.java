package com.example.restcountries.viewmodel;

import android.content.Context;

import android.view.View;
import androidx.annotation.NonNull;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableInt;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import com.example.restcountries.R;
import com.example.restcountries.utils.network.*;
import com.example.restcountries.model.Country;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;

public class CountriesDisplayViewModel extends Observable {

    public final Retrofit API = new Retrofit.Builder().baseUrl(NetworkingConstants.BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    public ObservableInt progressBar;
    public ObservableInt countryRecycler;
    public ObservableField<String> messageLabel;

    private List<Country> countryList;
    private Context context;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public CountriesDisplayViewModel(@NonNull Context context) {
        this.context = context;
        this.countryList = new ArrayList<>();
        this.progressBar = new ObservableInt(View.GONE);
        this.countryRecycler = new ObservableInt(View.GONE);
        initializeViews();
        fetchCountriesList();
    }

    private void initializeViews() {
        countryRecycler.set(View.GONE);
        progressBar.set(View.VISIBLE);
    }

    private void fetchCountriesList() {
        RestCountriesService service = API.create(RestCountriesService.class);
        Disposable disposable = service.listCountries()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<Country>>() {
                    @Override public void accept(List<Country> Response) throws Exception {
                        updateCountriesDataList(Response);
                        progressBar.set(View.GONE);
                        countryRecycler.set(View.VISIBLE);
                    }
                }, new Consumer<Throwable>() {
                    @Override public void accept(Throwable throwable) throws Exception {
                        messageLabel.set(context.getString(R.string.error_message_loading_countries));
                        progressBar.set(View.GONE);
                        countryRecycler.set(View.GONE);
                    }
                });

        compositeDisposable.add(disposable);
    }

    private void updateCountriesDataList(List<Country> countries) {
        HashMap<String,Country> countryHashMap = new HashMap<>();
        for (Country country:countries) {
            countryHashMap.put(country.getAlpha3Code(),country);
        }
        for (Country country:countries) {
            ArrayList<Country> neighbours = new ArrayList<>();
            for (String border:country.getBorders()) {
                neighbours.add(countryHashMap.get(border));
            }
            country.setNeighbourCountries(neighbours);
        }
        countryList.addAll(countries);
        setChanged();
        notifyObservers();
    }

    public void sortByNameDescending(View v){
        if (countryList.size() > 0) {
            Collections.sort(countryList, new Comparator<Country>() {
                @Override
                public int compare(final Country object1, final Country object2) {
                    return object1.getName().compareTo(object2.getName());
                }
            });
        }
        Collections.reverse(countryList);
        setChanged();
        notifyObservers();
    }

    public void sortByNameAscending(View v){
        if (countryList.size() > 0) {
            Collections.sort(countryList, new Comparator<Country>() {
                @Override
                public int compare(final Country object1, final Country object2) {
                    return object1.getName().compareTo(object2.getName());
                }
            });
        }
        setChanged();
        notifyObservers();
    }

    public void sortByAreaSizeAscending(View v){
        if (countryList.size() > 0) {
            Collections.sort(countryList, new Comparator<Country>() {
                @Override
                public int compare(final Country object1, final Country object2) {
                    if(object1.getArea()>object2.getArea())
                        return 1;
                    else if(object1.getArea()<object2.getArea())
                        return -1;
                    else
                        return 0;
                }
            });
        }
        setChanged();
        notifyObservers();
    }

    public void sortByAreaSizeDescending(View v){
        if (countryList.size() > 0) {
            Collections.sort(countryList, new Comparator<Country>() {
                @Override
                public int compare(final Country object1, final Country object2) {
                    if(object1.getArea()>object2.getArea())
                        return -1;
                    else if(object1.getArea()<object2.getArea())
                        return 1;
                    else
                        return 0;
                }
            });
        }
        setChanged();
        notifyObservers();
    }

    public List<Country> getCountriesList() {
        return countryList;
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public void reset() {
        unSubscribeFromObservable();
        compositeDisposable = null;
        context = null;
    }
}